from django.conf.urls import url
from django.contrib import admin

from users.views import (
    Login, Dashboard, GetCodeDatails,
    UpdateCodeDetails, DeleteCodeDetail, Logout
)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^logout/$', Logout.as_view(), name='logout'),
    url(r'^api/codes$', GetCodeDatails.as_view()),
    url(r'^api/update/(?P<pk>[A-Za-z_0-9\-]+)/$', UpdateCodeDetails.as_view()),
    url(r'^api/delete/(?P<pk>[A-Za-z_0-9\-]+)/$', DeleteCodeDetail.as_view()),
    url(r'^', Dashboard.as_view(), name='dashboard'),
]
