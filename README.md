# Code Management
================================================================

# How to setup ?
There are two ways to install.

# You can install manually install library that are required to run this APP


### The pythonista way

Ensure that you have an updated version of pip

```
pip --version
```
Should atleast be 1.5.4

If pip version is less than 1.5.4 upgrade it
```
pip install -U pip
```

This will install latest pip

Ensure that you are in virtualenv
if not install virtual env
```
sudo pip install virtualenv
```
This will make install all dependencies to the virtualenv
not on your root

From the module folder install the dependencies. This also installs
the module itself in a very pythonic way.

```
pip install -r requirements.txt
```
## NOTE

You also have to export SECKET_KEY and DATABASE details
in your environment to run this app

Postgresql must be installed to use postgresql as a DATABASE
Any RDBMS canbe used to run this app.

if not, install postgres and its server-side extensions

```
export SECRET_KEY='YOUR SECRET KEY'
```

Before starting the app tables needs to be migrated.
To do so, run makemigration command.

```
python manage.py makemigration users
```

then create superuser

```
python manage.py createsuperuser
```

This will user managing django-admin panel where user can be add under manager app for logging code.

```
python manage.py migrate
```

Run app by

```
python manage.py runserver
```
