# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View

from users.decorators import auth_required, login, logout, UserAuthentication
from users.forms import UserLoginForm, CodeForm, Code
from users.serializers import CodeSerializer

from rest_framework import permissions, generics, exceptions

from datetime import datetime


class Login(View):
    template_name = 'login.html'
    form = UserLoginForm

    def get(self, request, *args, **kwargs):
        if 'authToken' in request.session.keys():
            return redirect(reverse("dashboard"))
        return render(request, self.template_name, {'form': self.form()})

    def post(self, request, *args, **kwargs):
        form = self.form(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            return redirect(reverse("dashboard"))
        return render(request, self.template_name, {'form': form})


class Logout(View):

    @method_decorator(auth_required())
    def get(self, request, user, *args, **kwargs):
        logout(request, user)
        return redirect(reverse("login"))


class Dashboard(View):
    template_name = 'dashboard.html'
    form = CodeForm

    @method_decorator(auth_required())
    def get(self, request, user, *args, **kwargs):
        return render(request, self.template_name, {
            'form': self.form(), 'user': user,
            'next_increament': Code.get_unique_increament()
        })

    @method_decorator(auth_required())
    def post(self, request, user, *args, **kwargs):
        form = self.form(data=request.POST)
        context = {
            'form': self.form(), 'user': user,
            'next_increament': Code.get_unique_increament() + 1
        }
        if form.is_valid():
            form.save()
            context.update({'code': form.get_instance()})
        context['saved'] = True
        return render(request, self.template_name, context)


class GetCodeDatails(generics.ListAPIView):
    authentication_classes = [UserAuthentication]
    permission_classes = [permissions.AllowAny]
    serializer_class = CodeSerializer

    def get_queryset(self):
        queryset = Code.objects.all()
        data = self.request.query_params
        if 'start_date' in data:
            queryset = queryset.filter(
                created__gte=datetime.strptime(data['start_date'], '%d/%m/%Y'))
        if 'end_date' in data:
            queryset = queryset.filter(
                created__lte=datetime.strptime(data['end_date'], '%d/%m/%Y'))
        return queryset


class UpdateCodeDetails(generics.UpdateAPIView):
    authentication_classes = [UserAuthentication]
    permission_classes = [permissions.AllowAny]
    serializer_class = CodeSerializer
    queryset = Code.objects.all()

    def put(self, request, *args, **kwargs):
        raise exceptions.MethodNotAllowed(request.method)


class DeleteCodeDetail(generics.DestroyAPIView):
    authentication_classes = [UserAuthentication]
    permission_classes = [permissions.AllowAny]
    serializer_class = CodeSerializer
    queryset = Code.objects.all()
