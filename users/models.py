# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.validators import MinValueValidator

from . import STATES, A_TYPE, D_TYPE


class User(models.Model):
    """
    Stores the basic information about user
    """
    email = models.EmailField(max_length=64, unique=True)
    password = models.CharField(max_length=512)
    last_login_time = models.DateTimeField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        try:
            current = User.objects.get(id=self.id)
            if (current.password != User.get_hashed_password(self.password)):
                self.password = User.get_hashed_password(self.password)
        except User.DoesNotExist:
            self.password = User.get_hashed_password(self.password)
        super(User, self).save(*args, **kwargs)

    @staticmethod
    def get_hashed_password(password):
        # Hash a password for the first time
        # Using bcrypt, the salt is saved into the hash itself
        import bcrypt
        salt = bcrypt.gensalt()
        return bcrypt.hashpw(str(password), salt)

    def check_password(self, password):
        """
        check hased password. Using bcrypt, the salt is saved into the
        hash itself
        """
        import bcrypt
        try:
            return bcrypt.hashpw(
                str(password), str(self.password)) == str(self.password)
        except Exception:
            # If Password has invalid Salt
            return False

    @classmethod
    def get_authenticated_user(cls, request):
        """ Decorator uses this method to get authenticated user
        """
        import json
        try:
            data = json.loads(request.session['authToken'])
            return cls.objects.get(email=data['email'], id=data['id'])
        except cls.DoesNotExist:
            return None

    def get_accessToken(self):
        """ Generate the accessToken, currently using simple JSON.
        JWT Token or any other strong algo can be used to generate
        this accessToken
        """
        import json
        return json.dumps({"email": self.email, 'id': self.id})

    @classmethod
    def validate_user(cls, request):
        """ This class method validate users form when he tries to login
        return accessToken will be saved in session.
        """
        data = request.POST.dict()
        try:
            user = cls.objects.get(email=data.get('email', '').lower())
            validated = user.check_password(data.get('password'))
            if validated:
                return user.get_accessToken(), "Hi %s!. You had been successfully authenticated." % operator.username  # noqa
            return None, "Invalid Password Provided."
        except cls.DoesNotExist:
            return None, 'Invalid Username Provided.'
        return None, 'Something went wrong! Please try again.'


class Code(models.Model):
    UNIQUE_BASE = 123
    reference_no = models.PositiveIntegerField(
        validators=[MinValueValidator(1)])
    state = models.CharField(
        max_length=16, choices=tuple([(x, x) for x in STATES]))
    a_type = models.CharField(
        max_length=2, choices=tuple([(x, x) for x in A_TYPE]))
    d_type = models.CharField(
        max_length=2, choices=tuple([(x, x) for x in D_TYPE]))
    code = models.CharField(max_length=7)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        try:
            Code.objects.get(id=self.id)
        except Code.DoesNotExist:
            self.code = Code.get_code(self.state, self.a_type, self.d_type)
        super(Code, self).save(*args, **kwargs)

    @classmethod
    def get_code(cls, state, a_type, d_type, unique=None):
        return '%s%s%s%s' % (
            state[:2], a_type, d_type,
            unique or Code.get_unique_increament()
        )

    @classmethod
    def get_unique_increament(cls):
        return cls.UNIQUE_BASE + cls.objects.all().count()
