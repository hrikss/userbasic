var _dash;
$(function () {
  	$("#datepicker1").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  	})

  	$("#datepicker2").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  	})

  	Dashboard.init()

    var selector = document.getElementById('codedata')
    selector.addEventListener("scroll", function(){
        var translate = "translate(0,"+this.scrollTop+"px)";
        this.querySelector("thead").style.transform = translate;
    });

});

Array.prototype.getOrderedNameKey = function (nameKey, actualnamekey) {
    for (var i=0; i < this.length; i++) {
        if (this[i][actualnamekey] === nameKey) {
            return this[i];
        }
    }
}

class Dashboard {
	constructor() {
		this.data_api = '/api/codes'
		this.update_api = '/api/update/'
		this.delete_api = '/api/delete/'
		this.tableContainer = $('#codeContainer')
		this._data = null
	}

	static init() {
		_dash = new Dashboard()
		_dash.initTableData()
	}

	initTableData(that=null) {
		var api = this.data_api
		if (that != null) {
			$(that).html('Searching' + loading).attr('disabled', true)
			var data = {
				'start_date': $('#start_date').val(),
				'end_date': $('#end_date').val()
			}
			if (data.start_date != '' && data.end_date != '') {
				api += '?start_date=' + data.start_date + '&end_date=' + data.end_date
			} else if (data.start_date != '' && data.end_date == ''){
				api += '?start_date=' + data.start_date
			} else if (data.end_date != '') {
				api += '?end_date' + data.end_date
			} else {
				alert('Please select date to search.')
				$(that).html('Search').attr('disabled', false)
				return false
			}		
		}
    	$.ajax({
        	type: "GET",
        	url: api,
        	success: function (response) {
        		_dash._data = response
        		_dash.update_table()	
        	},
        	complete: function (){
        		$(that).html('Search').attr('disabled', false)
        	},
    	})
	}

	update_table() {
        var resp = ''
        this._data.map((row, index) => {
        	resp += Dashboard.row_skeleton(row)
        })
        this.tableContainer.html(resp)		
	}

	reverse() {
		this._data.reverse()
		this.update_table()
	}

	edit(row_id) {
		var tr = document.getElementById('tr_' + row_id)
		tr.innerHTML = Dashboard.edit_skeleton(this._data.getOrderedNameKey(parseInt(row_id), 'id'))
	}

	cancel(row_id) {
		var tr = document.getElementById('tr_' + row_id)
		tr.innerHTML = Dashboard.row_skeleton(this._data.getOrderedNameKey(parseInt(row_id), 'id'))
	}

	delete(row_id) {
		$.ajax({
        	type: 'DELETE',
        	url: this.delete_api + row_id + '/',
        	headers: {
        		'Accept': 'application/json',
        		'Content-Type': 'application/json'
    		},
        	success: function (response) {
        		$('#tr_' + row_id).remove()
        	}
    	})
		return false		
	}

	save(that) {
		var id = that.id.replace('edit_save_', '') 
		if ($('#edit_ref_' + id).val() == '') {
			alert('Please enter reference no')
			return false
		}
		$(that).html('Saving ' + loading).attr('disabled', true)
		var data = JSON.stringify({'reference_no': parseInt($('#edit_ref_' + id).val())}) 
		$.ajax({
        	type: 'PATCH',
        	url: this.update_api + id + '/',
        	data: data,
        	headers: {
        		'Accept': 'application/json',
        		'Content-Type': 'application/json'
    		},
        	success: function (response) {
        		let tmp = _dash._data.getOrderedNameKey(response.id, 'id')
        		tmp.reference_no = response.reference_no
        		_dash.update_table()
        	},
        	complete: function (){$(that).html('Save').attr('disabled', false)},
        	error: function () {$(that).html('Save')}
    	})
		return false
	}

	static toogle_createview(view_id, clear=true) {
		$('.createview').hide(50)
		$('#' + view_id).show(100)
		if (clear) {
			$('#id_state').val('')
			$('#id_a_type').val('')
			$('#id_d_type').val('')
			$('#id_reference_no').val('')
		}
	}

	static search(that) {
  		var filter, table, tr, td, cell;
  		filter = that.value.toLowerCase();
  		table = document.getElementById("searchableTable");
  		tr = table.getElementsByTagName("tr");
  		for (cell = 0; cell < tr.length; cell++) {
    		td = tr[cell].getElementsByTagName("td")[Dashboard.get_column(that.name)];
    		if (td) {
      			if ((td.innerHTML).toLowerCase().indexOf(filter) > -1) {
        			tr[cell].style.display = "";
      			} else {
        			tr[cell].style.display = "none";
      			}
    		}       
  		}
	}

	static get_column(name) {
		return {'state': 0, 'a_type': 1, 'd_type': 2, 'code': 3, 'reference_no': 4}[name]
	}

	static row_skeleton(row) {
		var resp = '<tr id="tr_' + row.id + '">'
		resp += '<td style="width=20%">' + row.state + '</td>'
		resp += '<td style="width=10%">' + row.a_type + '</td>'
		resp += '<td style="width=10%">' + row.d_type + '</td>'
		resp += '<td style="width=10%">' + row.code + '</td>'
		resp += '<td style="width=20%">' + row.reference_no + '</td>'
		resp += '<td style="width=10%">' + row.created + '</td>'
		resp += '<td style="width=20%"><span class="edit" onclick="_dash.edit(' + row.id +')">Edit </span>/'
		resp += '<span class="delete" onclick="_dash.delete(' + row.id + ')">Delete</span></td></tr>'
		return resp
	}

	static edit_skeleton(row) {
		var resp = '<tr id="' + row.id + '">'
		resp += '<td>' + row.state + '</td>'
		resp += '<td>' + row.a_type + '</td>'
		resp += '<td>' + row.d_type + '</td>'
		resp += '<td>' + row.code + '</td>'
		resp += '<td><input type="number" min="1" max="99999999999999999999" required class="form-control" id="edit_ref_' + row.id +'" value="' + row.reference_no + '"></td>'
		resp += '<td>' + row.created + '</td>'
		resp += '<td><button type="submit" class="btn btn-primary" id="edit_save_' + row.id + '" onclick="_dash.save(this)">Save</button>'
		resp += '<span class="cancel" onclick="_dash.cancel(' + row.id + ')">Cancel</span></td>'
		resp += '</tr>'
		return resp
	}

	static export() {
		setTimeout(()=>{
			var html = document.querySelector("table").outerHTML;
			Dashboard.export_table_to_csv(html, "code_data.csv");
		})
		return false
	}

	static download_csv(csv, filename) {
    	var csvFile;
    	var downloadLink;
    	csvFile = new Blob([csv], {type: "text/csv"});
    	downloadLink = document.createElement("a");
    	downloadLink.download = filename;
    	downloadLink.href = window.URL.createObjectURL(csvFile);
    	downloadLink.style.display = "none";
	    document.body.appendChild(downloadLink);
    	downloadLink.click();		
	}


	static export_table_to_csv(html, filename) {
		var csv = [];
		var rows = document.querySelectorAll("table tr");
	
    	for (var i = 0; i < rows.length; i++) {
			var row = [], cols = rows[i].querySelectorAll("td, th");
		
        	for (var j = 0; j < cols.length; j++) 
            	row.push(cols[j].innerText);
        
			csv.push(row.join(","));		
		}
    	Dashboard.download_csv(csv.join("\n"), filename);
	}
}