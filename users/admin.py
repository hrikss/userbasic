# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from users.models import User, Code


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'created', 'last_login_time')
    search_fields = ('email',)


@admin.register(Code)
class CodeAdmin(admin.ModelAdmin):
    list_display = ('reference_no', 'code', 'state', 'a_type', 'd_type')
    search_fields = ('reference_no', 'code')
    list_filter = ('state', 'a_type', 'd_type')
