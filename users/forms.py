from django import forms

from users.models import Code, User


class UserLoginForm(forms.ModelForm):
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={
            'maxlength': '20',
        })
    )

    class Meta:
        model = User
        fields = ('email', 'password')

    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)
        for field in self.visible_fields():
            field.field.widget.attrs['class'] = 'form-control'
            field.field.widget.attrs['placeholder'] = field.label

    def clean(self):
        cleaned_data = self.cleaned_data
        email = cleaned_data.get('email')
        password = cleaned_data.get('password')

        users = User.objects.filter(email=email)
        if users.exists():
            user = users.get()
            if not user.check_password(password):
                self.add_error('password', 'Password entered is incorrect.')
            return
        self.add_error('email', 'User doesnot exists with this email')

    def get_user(self):
        return User.objects.get(email=self.cleaned_data['email'])


class CodeForm(forms.ModelForm):

    class Meta:
        model = Code
        fields = ('state', 'a_type', 'reference_no', 'd_type')

    def __init__(self, *args, **kwargs):
        super(CodeForm, self).__init__(*args, **kwargs)
        for field in self.visible_fields():
            if field.name == 'reference_no':
                field.field.widget.attrs['max'] = '99999999999999999999'
                field.field.widget.attrs['min'] = '1'
            field.field.widget.attrs['class'] = 'form-control'
            field.field.widget.attrs['placeholder'] = field.label
            field.field.widget.attrs['autocomplete'] = 'off'

    def get_instance(self):
        data = self.cleaned_data
        unique_code = Code.get_unique_increament() - 1
        return Code.objects.get(code=Code.get_code(
            data['state'], data['a_type'], data['d_type'], unique_code),)
