from django.shortcuts import redirect

from rest_framework.authentication import BaseAuthentication
from rest_framework import exceptions


def auth_required(methods={"GET": 0, "POST": 0, "PUT": 0, "DELETE": 0}):
    def wrap(view):
        def wrapper(request, *args, **kw):
            if 'authToken' in request.session.keys():
                from users.models import User
                user = User.get_authenticated_user(request)
                if user:
                    return view(request, user, *args, **kw)
            return redirect('/login/')
        return wrapper
    return wrap


class UserAuthentication(BaseAuthentication):
    def authenticate(self, request):
        if 'authToken' in request.session.keys():
            from users.models import User
            user = User.get_authenticated_user(request)
            if user:
                return (user, None)
        raise exceptions.AuthenticationFailed('Authentication Failed')


def login(request, user):
    request.session['authToken'] = user.get_accessToken()
    request.session.save()


def logout(request, user):
    del request.session['authToken']
    request.session.save()
