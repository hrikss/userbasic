from rest_framework import serializers

from users.models import Code


class CodeSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(format="%d/%m/%Y")

    class Meta:
        model = Code
        fields = '__all__'
